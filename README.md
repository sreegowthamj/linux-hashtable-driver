# linux-hashtable-driver

The hash tables are implemented in kernel space as devices “ht530-0” and “ht530-1”and managed
by a device driver “ht530_drv”. Each hash table is with 128 buckets and can be accessed as a device file.
The hash tables are created and the devices are added to Linux device file systems when the device driver is installed. The device driver should be implemented as a Linux
kernel module and enable the following file operations:

    open: to open a device (the device is “ht530-0” or “ht530-1”).
    
    write: if the input object has a non-zero data field, a ht_object is created and added it to the hash
    table. If an old object with the same key already exist in the hash table, it should be replaced with
    the new one. If the data field is 0, any existing object with the input key is deleted from the table.
    
    read: to retrieve an object based on an input key. If no such object exists in the table, -1 is
    returned and errno is set to EINVAL.
    
    Ioctl: a new command “dump” to dump all objects hashed to bucket n. If n is out of range, -1 is
    returned and errno is set to EINVAL.
    
    release: to close the descriptor of an opened device file.

