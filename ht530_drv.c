/*  
 *  hello-1.c - The simplest kernel module.
 */
#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/fs.h>
#include <linux/hashtable.h>

#define SUCCESS 0
#define DEVICE_NAME_0 "ht530-0"	/* Dev name as it appears in /proc/devices   */
#define DEVICE_NAME_1 "ht530-1"	/* Dev name as it appears in /proc/devices   */

#define HT0_SIZE 7
#define HT1_SIZE 7

static DEFINE_HASHTABLE(ht530_tbl_0, HT0_SIZE);

#define BUF_LEN 80		/* Max length of the message from the device */


static int Major0, Major1;		/* Major number assigned to our device driver */
static int Device_Open = 0;	/* Is device open? */
static char msg[BUF_LEN];	/* The msg the device will give when asked */
static char *msg_Ptr;

static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

static struct file_operations fops_0 = {
        .owner = THIS_MODULE,           /* Owner */
	.read = device_read,
	.write = device_write,
	.open = device_open,
	.release = device_release
};

static struct file_operations fops_1 = {
        .owner = THIS_MODULE,           /* Owner */
	.read = device_read,
	.write = device_write,
	.open = device_open,
	.release = device_release
};


typedef struct ht_object {
        int key;
        int data;
        struct hlist_node node;

} ht_object_t;

struct ht_dev {
	struct cdev cdev;               /* The cdev structure */
	char name[20];                  /* Name of device*/
	char in_string[256];			/* buffer for the input string */
	int current_write_pointer;
} *ht_devp;



int init_module(void)
{
        int i;
        int bkt;
        struct ht_object* curr;
        ht_object_t obj[50];
        printk(KERN_INFO "Hello world 1.\n");
        Major0 = register_chrdev(0, DEVICE_NAME_0, &fops_0);
        Major1 = register_chrdev(0, DEVICE_NAME_1, &fops_1);
	if (Major0 < 0) {
	        printk(KERN_ALERT "Registering char device failed with %d\n", Major0);
	        return Major0;
	}
	if (Major1 < 0) {
 	        printk(KERN_ALERT "Registering char device failed with %d\n", Major1);
	        return Major1;
	}
        
	printk(KERN_INFO "I was assigned major number %d, %d \n", Major0, Major1);

        hash_init(ht530_tbl_0);
        for (i = 0; i<50; i++) {
              obj[i].key = i;
              obj[i].data = i*2;

              printk(KERN_INFO "address of obj.node = %p \n", &obj[i].node);
              hash_add(ht530_tbl_0, &obj[i].node, obj[i].key);
        }

        hash_for_each(ht530_tbl_0, bkt, curr, node) {
                printk(KERN_INFO "data=%d is in bucket %d\n", curr->data, bkt);
                hash_del(&curr->node);
        }

	/* 
	 * A non 0 return means init_module failed; module can't be loaded. 
	 */
	return 0;
}

void cleanup_module(void)
{
	printk(KERN_INFO "Goodbye world 1.\n");
        unregister_chrdev(Major0, DEVICE_NAME_0);
        unregister_chrdev(Major1, DEVICE_NAME_1);
}


/* 
 * Called when a process tries to open the device file, like
 * "cat /dev/mycharfile"
 */

static int device_open(struct inode *inode, struct file *file)
{
	static int counter = 0;

	if (Device_Open)
		return -EBUSY;

	Device_Open++;
	sprintf(msg, "I already told you %d times Hello world!\n", counter++);
	msg_Ptr = msg;
	try_module_get(THIS_MODULE);

	return SUCCESS;
}

/* 
 * Called when a process closes the device file.
 */
static int device_release(struct inode *inode, struct file *file)
{
	Device_Open--;		/* We're now ready for our next caller */

	/* 
	 * Decrement the usage count, or else once you opened the file, you'll
	 * never get get rid of the module. 
	 */
	module_put(THIS_MODULE);

	return 0;
}


/* 
 * Called when a process, which already opened the dev file, attempts to
 * read from it.
 */
static ssize_t device_read(struct file *filp,	/* see include/linux/fs.h   */
			   char *buffer,	/* buffer to fill with data */
			   size_t length,	/* length of the buffer     */
			   loff_t * offset)
{
	/*
	 * Number of bytes actually written to the buffer 
	 */
	int bytes_read = 0;

	/*
	 * If we're at the end of the message, 
	 * return 0 signifying end of file 
	 */
	if (*msg_Ptr == 0)
		return 0;

	/* 
	 * Actually put the data into the buffer 
	 */
	while (length && *msg_Ptr) {

		/* 
		 * The buffer is in the user data segment, not the kernel 
		 * segment so "*" assignment won't work.  We have to use 
		 * put_user which copies data from the kernel data segment to
		 * the user data segment. 
		 */
          //put_user(*(msg_Ptr++), buffer++);

		length--;
		bytes_read++;
	}

	/* 
	 * Most read functions return the number of bytes put into the buffer
	 */
	return bytes_read;
}

/*  
 * Called when a process writes to dev file: echo "hi" > /dev/hello 
 */
static ssize_t
device_write(struct file *filp, const char *buff, size_t len, loff_t * off)
{
	printk(KERN_ALERT "Sorry, this operation isn't supported.\n");
	return -EINVAL;
}
