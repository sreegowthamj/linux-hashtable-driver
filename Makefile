MODULE = ht530_drv
obj-m += $(MODULE).o

# Check that given variables are set and all have non-empty values,
# die with an error otherwise.
#
# Params:
#   1. Variable name(s) to test.
#   2. (optional) Error message to print.
check_defined = \
    $(strip $(foreach 1,$1, \
        $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
    $(if $(value $1),, \
      $(error Undefined $1$(if $2, ($2))))

$(call check_defined, CROSS_COMPILE, SDKTARGETSYSROOT, ARCH)

###################################################################

all: compile

compile:
	make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILE) -C $(SDKTARGETSYSROOT)/usr/src/kernel M=$(PWD) modules
clean:
	@make -C $(SDKTARGETSYSROOT)/usr/src/kernel M=$(PWD) clean 1> /dev/null

flash: compile
	scp $(MODULE).ko  root@192.168.1.5:/home/root/ 
